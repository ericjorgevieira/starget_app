import React from 'react';
import { AsyncStorage, Alert } from 'react-native';

export default {
    auth: async function(){
        var date = +new Date();
        try{
            await AsyncStorage.setItem('@accessToken', date.toString());
        }catch(e){
            console.log(e)
        }
    },
    isAuth: async function(){
        try {
            return await this.getAuthToken();
          } catch (error) {
            console.log(error);
            return false;
          }
    },
    getAuthToken: async function(){
        try {
            var token = await AsyncStorage.getItem('@accessToken');
            return token;
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    listProjects: async function(){
        try {
            var token = await this.getAuthToken();
            return JSON.parse(await AsyncStorage.getItem('@' + token + '_projects'));
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    getProject: async function(slug){
        var projects = await this.listProjects();
        if(projects && projects.length > 0){
            var project = projects.filter(p => {
                return p.slug == slug;
            })
            return project[0];
        }
        return null;
    },
    saveProject: async function(project){
        try {
            var token = await this.getAuthToken();
            var projects = await this.listProjects();
            if(!projects){
                projects = [];
            }
            var projectKey = Object.keys(projects).filter(p => {
                return projects[p].slug == project.slug;
            })
            if(projectKey[0]){
                projects[projectKey[0]] = project;
            }else{
                projects.push(project);
            }
            await AsyncStorage.setItem('@' + token + '_projects', JSON.stringify(projects));
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    hasProject: async function(slug){
        var projects = await this.listProjects();
        if(projects && projects.length > 0){
            var hasProject = projects.filter(p => {
                return p.slug == slug;
            })
            return hasProject && hasProject.length > 0;
        }
        return false;
    },
    deleteProjects: async function(){
        var token = await this.getAuthToken();
        await AsyncStorage.removeItem('@' + token + '_projects');
    },
    deleteProject: async function(project)
    {
        var projects = await this.listProjects();
        var token = await this.getAuthToken();

        if(projects && projects.length > 0){
            projects = projects.filter(p => {
                return p.slug != project.slug;
            })
            await AsyncStorage.setItem('@' + token + '_projects', JSON.stringify(projects));
        }
        return null;
    }
}