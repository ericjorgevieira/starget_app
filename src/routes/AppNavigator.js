import React from 'react';
import { StackNavigator } from 'react-navigation';
import { StatusBar, Platform, View } from 'react-native';
import WalkthroughScreen from '../pages/main/WalkthroughScreen';
import HomeScreen from '../pages/main/HomeScreen';
import AboutScreen from '../pages/main/AboutScreen';
import BookScreen from '../pages/main/BookScreen';
import RegisterScreen from '../pages/main/RegisterScreen';
import QuizScreen from '../pages/main/QuizScreen';
import ResultScreen from '../pages/main/ResultScreen';
import TermsScreen from '../pages/main/TermsScreen';
import Ionicons from 'react-native-vector-icons/Ionicons';

const MainNavigator = StackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            header: null
        }
    },
    About: {
        screen: AboutScreen,
        navigationOptions: {
            header: null
        }
    },
    Book: {
        screen: BookScreen,
        navigationOptions: {
            header: null
        }
    },
    Register:{ 
        screen: RegisterScreen,
        navigationOptions: {
            header: null
        }
    },
    Quiz:{ 
        screen: QuizScreen,
        navigationOptions: {
            header: null
        }
    },
    Result:{ 
        screen: ResultScreen,
        navigationOptions: {
            header: null
        }
    }
});

const AppNavigator = StackNavigator({
    Terms: {
        screen: TermsScreen,
        navigationOptions: {
            header: null
        }
    },
    Main: {
        screen: MainNavigator,
        navigationOptions: {
            header: null
        }
    },
    Walkthrough: {
        screen: WalkthroughScreen,
        navigationOptions: {
            header: null
        }
    },
}, {
        initialRouteName: 'Terms',
        headerStyle: { backgroundColor: '#440276' },
        cardStyle: {
            paddingTop: Platform.OS === 'ios' ? 0 : 0
        }
    });

export default AppNavigator;
