import React from 'react';
import { BackHandler, View, ScrollView, Text, StatusBar, Platform, ActivityIndicator } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import User from '../../models/User';
import Config from '../../utils/Config';

export default class TermsScreen extends React.Component {
    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props, context) {
        super(props, context);
        this.state = {showTerms: false}
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        User.isAuth().then((value) => {
            if(value){
                this.props.navigation.navigate('Main');
            }else{
                this.setState({showTerms: true})
            }
        });
        StatusBar.setBarStyle('light-content');
    }

    onBackButtonPressAndroid = () => {
        return true;
    };

    componentDidMount(){
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    componentWillUnmount(){
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    goToMain()
    {
        this.props.navigation.navigate('Main');
    }

    render() {
        if(this.state.showTerms){
            return (
                <View style={{flex: 1}}>
                    <View style={{ height: 65, width: '100%', alignItems: 'center', backgroundColor: '#440276'}}>
                        <Text onPress={() => this.goToMain()} style={{position: 'absolute', right: 40, top: Platform.OS == 'ios' ? 33 : 20, color: '#fff', fontSize: 16 }}>Aceitar</Text>
                        <Feather color={'#fff'} style={{ position: 'absolute', padding: 10, right: 5, top: Platform.OS == 'ios' ? 20 : 10, zIndex: 100 }} 
                            size={25} name={'chevron-right'} onPress={() => this.goToMain()}>
                        </Feather>
                        <Text style={{ width: '100%', textAlign: 'center', color: '#fff', fontFamily: 'font-bold', fontSize: 18, marginTop: Platform.OS == 'ios' ? 35 : 25 }}>Termos</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <ScrollView style={{flex: 1, padding: 20}}>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 24, textAlign: 'center', marginTop: 10}}>
                                Termos de Uso e Política de Privacidade
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                1. Aceitação
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Estes Termos e Condições de Uso (“Termos de Uso”) regulam o acesso e utilização dos serviços prestados através da ANJA HEALTH CLINICA LTDA, empresa limitada inscrita no CNPJ/MF sob o nº 37.449.202/0001-25, com sede na Rua José Seabra, nº 11, Quadra 11, Bloco C – Capim Macio, Natal/RN, disponibilizados através da Aplicação de internet STARGET. 
                                A utilização dos serviços oferecidos pelo Aplicativo dependerá necessariamente de aceitação do usuário destes Termos de Uso e da Política de Privacidade e Disposições Gerais. 
                                O Usuário deverá ler atentamente os termos ora apresentados, estando plenamente ciente e de acordo com todas as condições ora estabelecidas. O presente Termos de Uso terá força vinculante e imediata entre as partes, entendendo-se que com a utilização do Aplicativo, o Usuário compreendeu e aceitou todas as cláusulas.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                2 Condições de acesso e utilização do Aplicativo
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 17, textAlign: 'left', marginTop: 10}}>
                                2.1 Acesso 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A utilização do Aplicativo e de suas ferramentas exige o prévio registro do Usuário, sendo obrigação do Usuário informar todos os dados solicitados no momento do cadastro no Aplicativo, com precisão e veracidade, sendo vedada a inserção de dados falsos e de terceiros.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O Aplicativo concede ao Usuário uma licença pessoal, limitada, temporária, mundial, revogável, não exclusiva e intransferível de uso do aplicativo, sem cobrança de remuneração do Usuário de qualquer natureza.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 17, textAlign: 'left', marginTop: 10}}>
                                2.2 Utilização
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A utilização do Aplicativo pelo Usuário possui caráter pessoal e intransferível e está autorizada unicamente para fins lícitos relacionados ao propósito a que o aplicativo se destina, de acordo com estes Termos de Uso.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                É de responsabilidade exclusiva do Usuário a escolha de senha para verificação de seu acesso ao Aplicativo, devendo mantê-la em segredo e se abster de fornecê-la a terceiros, além de utilizar as tecnologias adequadas para proteger a sua privacidade perante terceiros.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 17, textAlign: 'left', marginTop: 10}}>
                                2.3 Propriedade Intelectual
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O Usuário está ciente de que os direitos sobre todos os componentes do Aplicativo, tais como arquivos de dados, textos, programas, arquivos de áudio, fotografias, vídeos, imagens, e outros, a estes não se limitando (coletivamente denominado “Conteúdo”), são de propriedade do STARGET, ou de terceiros, reproduzidos sob autorização, quando necessária, estando todo o Conteúdo protegido por direitos de propriedade intelectual, estejam esses registrados ou não.  
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                3. Exclusão de garantias e de responsabilidade
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O STARGET não garante que as informações disponibilizadas no Aplicativo estarão sempre disponíveis, sendo certo que elas também poderão ser excluídas, a qualquer momento, independentemente de qualquer aviso. Em razão disso, compete única e exclusivamente ao Usuário manter cópia de todas as informações disponibilizadas no Aplicativo que assim desejar.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O STARGET não será, em hipótese alguma, responsável por quaisquer danos decorrentes da interrupção de acesso ao Aplicativo, independentemente do motivo que cause essa suspensão, quer ocorra por culpa de terceiros ou não.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O usuário tem plena ciência e aceita que o prognóstico resultante da atividade desempenhada pelo Aplicativo constitui obrigação de meio, portanto, não sendo garantido qualquer resultado certo e/ou determinado, tampouco garantia de êxito nas suas pretensões.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                4. Duração e Finalização
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O acesso ao Aplicativo estará disponível por tempo indeterminado, podendo, a critério exclusivo de seus gerenciadores, a qualquer momento e independentemente de motivo ou aviso prévio, excluir perfis, negar, suspender ou descontinuar, temporária ou definitivamente, o acesso de Usuários ao Aplicativo, sem que seja devida qualquer indenização ou compensação a referidos Usuários.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                POLÍTICA DE PRIVACIDADE
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 17, textAlign: 'left', marginTop: 10}}>
                                1. Dados fornecidos, coletados, usados e armazenados
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Esse Aplicativo coleta e armazena todas as informações inseridas ativamente pelo Usuário no Aplicativo, a exemplo de seus dados pessoais (incluindo, mas não se limitando a, nome e número do CPF) e demais informações cadastrais (como endereço de e-mail e número de telefone). Além disso, também poderão ser armazenadas, automaticamente, algumas informações a partir do momento em que o Aplicativo é iniciado, tais como: 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                - Dados de geolocalização;
                                - Informações sobre o número IP, data e hora de inicialização do Aplicativo.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O Aplicativo também poderá utilizar outras tecnologias, como cookies, pixel tags, beacons e local shared objects, para coletar informações do Usuário e melhorar sua experiência de navegação. Alguns desses recursos podem ser bloqueados pelo Usuário quando de sua utilização do Aplicativo. Neste último caso, no entanto, o Usuário deve estar ciente de que algumas funções do Aplicativo poderão não funcionar corretamente.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Desta forma, por meio do aceite destes Termos, o Usuário se declara ciente das informações coletadas por meio do Aplicativo e manifesta consentimento livre, consciente, expresso e informado com relação à coleta de tais informações.
                                As informações constantes no Aplicativo poderão ser repassadas a terceiros, de forma gratuita, ou não, e utilizadas para fins estatísticos e publicitários, desde que anonimizados, ou seja, garantindo que não será possível a identificação do Usuário por meio dos dados fornecidos, no que o Usuário, desde logo, dá o consentimento livre, expresso e informado para tanto, nos termos exigidos pela Lei Nº 12.965/2014, Marco Civil da Internet.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                DISPOSIÇÕES GERAIS
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                1. Legislação e Foro
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Estes Termos serão regidos, interpretados e executados de acordo com as leis da República Federativa do Brasil, independentemente dos conflitos dessas leis com leis de outros estados ou países, sendo competente o Foro de Natal, para dirimir qualquer dúvida decorrente deste instrumento. O Usuário consente, expressamente, com a competência desse juízo, e renuncia, neste ato, à competência de qualquer outro foro, por mais privilegiado que seja ou venha a ser.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                2. Atualização dos termos de uso e política de privacidade
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Qualquer alteração com relação aos termos de uso e a política de privacidade desse Aplicativo será comunicada previamente por meio do próprio Aplicativo sobre tais alterações, devendo deixar de utilizar o Aplicativo caso não concordem com quaisquer modificações.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                3. Contato
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10, paddingBottom: 60}}>
                                Caso seja necessário contatarmos você, o contato será realizado por meio do endereço de e-mail informado quando de seu cadastro no Aplicativo, ou por meio de mensagens disponibilizadas no próprio Aplicativo.
                                Em caso de dúvidas, sugestões ou reclamações, o Usuário pode contatar o STARGET por meio do seu endereço de e-mail: stargetbussiness@gmail.com.
                            </Text>

                        </ScrollView>
                    </View>
                </View>
            );
        }else{
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#440276'}}>
                    <ActivityIndicator color={'#fff'} size={'large'}></ActivityIndicator>
                </View>
            )
        }
    }
}