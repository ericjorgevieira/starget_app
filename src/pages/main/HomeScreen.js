import React from 'react';
import { BackHandler, Text, View, TouchableOpacity, AsyncStorage, ActivityIndicator, Alert, ScrollView, Image, StatusBar } from 'react-native';
import Config from '../../utils/Config';
import User from '../../models/User';
import logo from '../../assets/images/logo-transparent.png';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Sidebar from './includes/Sidebar';
import iconIndividual from '../../assets/images/individual.png';
import iconStartup from '../../assets/images/startup.png';
import iconInstitutional from '../../assets/images/institutional.png';

export default class HomeScreen extends React.Component {
    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props, context) {
        super(props, context);

        this.state = {loading: true, showNewProjectButtons: false, showSidebar: false, projects: null};
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload => {
            this.loadProjects();
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        });
    }

    componentWillMount()
    {
        // AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove)
        this.authUser();
        // this.checkIfHasOpenedQuiz();
        
    }

    componentWillUnmount()
    {
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
        this._navListener.remove();
    }

    componentDidMount()
    {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    onBackButtonPressAndroid = () => 
    {
        BackHandler.exitApp();
        return true;
    };

    async authUser()
    {
        if(!await User.isAuth()){
            User.auth();
        }
        this.loadProjects();
    }

    async loadProjects()
    {   
        var projects = await User.listProjects();
        this.setState({loading: false, showNewProjectButtons: (projects && projects.length > 0) ? false : true, projects: projects});
    }

    async checkIfHasOpenedQuiz()
    {
        try{
            var hasOpenedQuiz = await AsyncStorage.getItem('hasOpenedQuiz');
            if(hasOpenedQuiz){
                var slug = await AsyncStorage.getItem('openedQuizProject');
                this.props.navigation.navigate('Quiz', {slug: slug});
            }else{
                this.setState({loading: false})
            }
        }catch(e){
            console.log(e)
        }
    }

    openProject(project)
    {
        if(project.endedQuiz){
            this.props.navigation.navigate('Result', {slug: project.slug});
        }else{
            this.props.navigation.navigate('Quiz', {slug: project.slug});
        }
    }

    askDeleteProject(project)
    {
        Alert.alert(
            'Remover Projeto',
            'Deseja remover o projeto "' + project.projectName + '"?',
            [
              {text: 'Não', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'Sim', onPress: () => this.deleteProject(project)},
            ],
            { cancelable: false }
          )
    }

    async deleteProject(project)
    {
        this.setState({loading: true});
        await User.deleteProject(project);
        await this.loadProjects();
        this.setState({loading: false})
    }

    renderProjects()
    {
        if(this.state.projects && this.state.projects.length > 0){
            return this.state.projects.map(p => {
                return (
                    <TouchableOpacity onLongPress={() => this.askDeleteProject(p)} onPress={() => this.openProject(p)} key={p.slug}
                        style={{width: '100%', height: 70, backgroundColor: '#ff5000', borderRadius: 5, alignItems: 'flex-start', justifyContent: 'center', marginTop: 20}}>
                        <Text style={{fontSize: 22, color: '#fff', marginLeft: 10, fontWeight: 'bold'}}>{ p.projectName }</Text>
                    </TouchableOpacity>
                )
            })
        }
    }
    
    goToRegister(type)
    {
        this.props.navigation.navigate('Register', {type: type});
    }

    render()
    {

        var mainButtons = (
            <View style={{width: '100%', height: '70%', alignItems: 'center', justifyContent: 'center', marginTop: 20}}>
                <Text style={{fontSize: 22, color: '#440276'}}>Meus Projetos</Text>

                <View style={{width: '100%', minHeight: 400, padding: 40, alignItems: 'center'}}>
                    {this.renderProjects()}

                    <TouchableOpacity onPress={() => this.setState({showNewProjectButtons: true})}
                        style={{width: 70, height: 70, borderWidth: 2, borderColor: '#440276', borderRadius: 50, alignItems: 'center', justifyContent: 'center', marginTop: 20}}>
                        <Text style={{fontSize: 36, color: '#ff5000', fontWeight: 'bold'}}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )

        var newProjectButtons = (
            <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                <Text style={{fontSize: 22, color: '#440276'}}>Como deseja se cadastrar?</Text>

                <ScrollView style={{width: '100%', padding: 20}}>
                    <TouchableOpacity onPress={() => this.goToRegister('individual')}
                        style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                        <Image style={{width: 120, height: 120, resizeMode: 'contain'}} source={iconIndividual} />
                        <Text style={{fontSize: 18, color: '#440276', fontWeight: 'bold'}}>Pessoa Física</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.goToRegister('startup')}
                        style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                        <Image style={{width: 120, height: 120, resizeMode: 'contain'}} source={iconStartup} />
                        <Text style={{fontSize: 18, color: '#440276', fontWeight: 'bold'}}>Startup</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.goToRegister('institucional')}
                        style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                        <Image style={{width: 120, height: 120, resizeMode: 'contain'}} source={iconInstitutional} />
                        <Text style={{fontSize: 18, color: '#440276', fontWeight: 'bold'}}>Institucional</Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => this.setState({showResultModal: true})}
                        style={{width: '100%', height: 50, borderWidth: 1, borderColor: '#440276', borderRadius: 10, 
                        alignItems: 'center', justifyContent: 'center', marginTop: 60}}>
                        <Text style={{fontSize: 18, color: '#440276', fontWeight: 'bold'}}>Resultado Exemplo</Text>
                    </TouchableOpacity> */}
                </ScrollView>
            </View>
        )

        var topBar = (
                <View style={{width: '100%', height: 60, alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 0}}>
                    <TouchableOpacity style={{position: 'absolute', left: 10, padding: 10}} onPress={() => this.setState({showSidebar: true})}>
                        <Ionicons name={'ios-menu'} size={30} color={'#440276'} />
                    </TouchableOpacity>
                    <Image source={logo} style={{width: 120, height: 120, resizeMode: 'contain'}} />
                </View>
        )
        
        if(this.state.projects && this.state.projects.length > 0 && this.state.showNewProjectButtons)
        {
            topBar = (
                <View style={{width: '100%', height: 60, alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 0}}>
                    <TouchableOpacity style={{position: 'absolute', left: 5, padding: 10}} onPress={() => this.setState({showSidebar: true})}>
                        <Feather color={'#440276'} size={25} name={'chevron-left'} onPress={() => this.setState({showNewProjectButtons: false})} />
                    </TouchableOpacity>
                    <Image source={logo} style={{width: 120, height: 120, resizeMode: 'contain'}} />
                </View>
            )
        }

        if(this.state.loading){
            return (
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator size="large" color="#440276"></ActivityIndicator>
                </View>
            )
        }else{
            return (
                <View style={{flex: 1, backgroundColor: '#fff'}}>
                    <StatusBar barStyle="light-content" />
                    
                    {topBar}

                    <View style={{flex: 1, marginTop: 80}}>
                        {this.state.showNewProjectButtons ? newProjectButtons : mainButtons}
                    </View>

                    <Sidebar show={this.state.showSidebar} close={() => this.setState({showSidebar: false})} navigation={this.props.navigation} reloadProjects={() => this.loadProjects()}></Sidebar>

                </View>
            )
        }

    }

}