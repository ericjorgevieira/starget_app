import React from 'react';
import { Platform, Text, View, TouchableOpacity, Image, Slider, ActivityIndicator, Alert, ScrollView, AsyncStorage } from 'react-native';
import { CheckBox } from 'react-native-elements'
import Config from '../../utils/Config';
import Feather from 'react-native-vector-icons/Feather';
import User from '../../models/User';
import individualData from '../../data/individuo';
import startupData from '../../data/startup';
import institucionalData from '../../data/colaborador';
import bkgCircle from '../../assets/images/bkg-circle.png';

export default class QuizScreen extends React.Component {

    constructor(props, context){
        super(props, context);
        const { navigation } = this.props;
        this.state = {slug: navigation.getParam('slug'), project: null, data: {}, loading: false, answers: [], actualQuestion: null};
    }

    async componentWillMount()
    {
        this.setHasOpenedQuiz();
        this.loadProject();
    }

    async setHasOpenedQuiz()
    {
        await AsyncStorage.setItem('hasOpenedQuiz', "true");
        await AsyncStorage.setItem('openedQuizProject', this.state.slug);
    }

    async loadProject()
    {
        try{
            var project = await User.getProject(this.state.slug);
            if(project){
                var data = {};
                switch(project.type){
                    case 'individual':
                        data = individualData;
                        break;
                    case 'startup':
                        data = startupData;
                        break;
                    case 'institucional':
                        data = institucionalData;
                        break
                }
                var answers = [];
                var questions = [];
                data.dimensoes.map(d => {
                    d.perguntas.map(p => {
                        answers.push({idPergunta: p.idPergunta, idDimensao: d.idDimensao, resposta: null});
                    })
                })
                data.dimensoes.map(d => {
                    d.perguntas.map(p => {
                        questions.push({...p, idDimensao: d.idDimensao});
                    })
                })
                this.setState({project: project, data: data, questions: questions, answers: answers});
                this.setCurrentQuestion();
            }else{
                Alert.alert('Erro', 'Projeto não encontrado.');
            }
        }catch(e){
            console.log(e)
            Alert.alert('Erro', 'Erro ao carregar projeto.');
        }
    }

    setCurrentQuestion()
    {
        var actualQuestionId = (this.state.actualQuestion) ? this.state.actualQuestion.idPergunta : this.state.questions[0].idPergunta;
        var isAnswered = this.state.answers.filter(a => {
            return (a.idPergunta == actualQuestionId && a.resposta);
        })
        if(!isAnswered || isAnswered.length == 0){
            var actualQuestion = this.state.questions.filter(q => {
                return q.idPergunta == actualQuestionId;
            })
            this.setState({actualQuestion: actualQuestion[0]});
        }else{
            if(!this.isLastQuestion()){
                var nextQuestion = this.state.questions.filter(q => {
                    return q.idPergunta == (actualQuestionId + 1);
                })
                this.setState({actualQuestion: nextQuestion[0]});
            }
        }
    }

    isLastQuestion(questionId)
    {
        var question = this.state.questions.filter(q => {
            return q.idPergunta == questionId;
        })
        if(question[0] && question[0].idPergunta == this.state.questions.length){
            return true;
        }
        return false;
    }

    setAnswer(idPergunta, resposta)
    {
        var answers = this.state.answers;
        answers.map(a => {
            if(a.idPergunta == idPergunta){
                a.resposta = resposta;
            }
            return a;
        })
        this.setState({answers: answers});
    }

    confirmQuestion()
    {
        var question = this.state.answers.filter(d => {
            return d.idPergunta == this.state.actualQuestion.idPergunta;
        })
        if(question[0] && question[0].resposta){
            this.setCurrentQuestion();
        }else{
            Alert.alert('Aviso', 'Você deve selecionar uma resposta para continuar!');
        }
    }

    getAnswer(questionId)
    {
        var answer = this.state.answers.filter(a => {
            return a.idPergunta == questionId;
        })
        return answer[0].resposta;
    }

    async finalizeQuiz()
    {
        try{
            var project = await User.getProject(this.state.slug);
            project['answers'] = this.state.answers;
            project['endedQuiz'] = true;
            await User.saveProject(project);
            this.props.navigation.goBack();
            this.props.navigation.navigate('Result', {slug: project.slug});
            //Exibe modal/tela com a estrela e o resultado
        }catch(e){
            console.log(e)
        }
    }

    promptToGoBack()
    {
        Alert.alert(
            'Sair do Questionário',
            'Deseja realmente sair? Você deverá preencher todas as perguntas novamente quando voltar.',
            [
              {text: 'Não', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'Sim', onPress: () => this.props.navigation.goBack()},
            ],
            { cancelable: false }
        )
    }

    renderQuestion()
    {
        var styleSelected = (resposta) => {
            if(resposta == this.getAnswer(this.state.actualQuestion.idPergunta)){
                return {flex: 1, flexWrap: 'wrap', backgroundColor: '#ff5000', borderWidth: 1, borderColor: '#ff5000', borderRadius: 5, marginLeft: 20, marginRight: 20, marginTop: 10, justifyContent: 'center', alignItems: 'center'}
            }else{
                return {flex: 1, flexWrap: 'wrap', backgroundColor: '#f4f4f4', borderWidth: 1, borderColor: '#dadada', borderRadius: 5, marginLeft: 20, marginRight: 20, marginTop: 10, justifyContent: 'center', alignItems: 'center'}
            }
        }

        var textStyleSelected  = (resposta) => {
            if(resposta == this.getAnswer(this.state.actualQuestion.idPergunta)){
                return {fontSize: 16, textAlign: 'center', color: '#fff', fontWeight: 'bold', marginLeft: 10};
            }else{
                return {fontSize: 16, textAlign: 'center', color: '#440276', marginLeft: 10};
            }
        }

        var nextButton = (
            <TouchableOpacity style={{width: '60%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: '#ff5000', marginTop: 30, borderRadius: 5}} 
                onPress={() => this.confirmQuestion()}>
                <Text style={{fontSize: 22, color: '#fff', padding: 10}}>Seguinte</Text>
            </TouchableOpacity>
        )

        if(this.state.actualQuestion && this.isLastQuestion(this.state.actualQuestion.idPergunta)){
            nextButton = (
                <TouchableOpacity style={{width: '60%', alignSelf: 'center',  alignItems: 'center', justifyContent: 'center', backgroundColor: '#ff5000', marginTop: 30, borderRadius: 5}} 
                    onPress={() => this.finalizeQuiz()}>
                    <Text style={{fontSize: 22, color: '#fff', padding: 10}}>Finalizar</Text>
                </TouchableOpacity>
            )
        }

        if(this.state.actualQuestion){
            return (
                <View style={{width: '100%', marginTop: 30, backgroundColor: '#fff'}} key={this.state.actualQuestion.idPergunta}>

                    <View style={{width: '100%', height: 60}}>
                        <TouchableOpacity style={styleSelected(this.state.actualQuestion.respostas[0].valor)}
                            onPress={() => this.setAnswer(this.state.actualQuestion.idPergunta, this.state.actualQuestion.respostas[0].valor)}>
                            <Text numberOfLines={2} style={textStyleSelected(this.state.actualQuestion.respostas[0].valor)}>{this.state.actualQuestion.respostas[0].alternativa}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{width: '100%', height: 60}}>
                        <TouchableOpacity style={styleSelected(this.state.actualQuestion.respostas[1].valor)}
                            onPress={() => this.setAnswer(this.state.actualQuestion.idPergunta, this.state.actualQuestion.respostas[1].valor)}>
                            <Text numberOfLines={2} style={textStyleSelected(this.state.actualQuestion.respostas[1].valor)}>{this.state.actualQuestion.respostas[1].alternativa}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{width: '100%', height: 60}}>
                        <TouchableOpacity style={styleSelected(this.state.actualQuestion.respostas[2].valor)}
                            onPress={() => this.setAnswer(this.state.actualQuestion.idPergunta, this.state.actualQuestion.respostas[2].valor)}>
                            <Text numberOfLines={2} style={textStyleSelected(this.state.actualQuestion.respostas[2].valor)}>{this.state.actualQuestion.respostas[2].alternativa}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{width: '100%', height: 60}}>
                        <TouchableOpacity style={styleSelected(this.state.actualQuestion.respostas[3].valor)}
                            onPress={() => this.setAnswer(this.state.actualQuestion.idPergunta, this.state.actualQuestion.respostas[3].valor)}>
                            <Text numberOfLines={2} style={textStyleSelected(this.state.actualQuestion.respostas[3].valor)}>{this.state.actualQuestion.respostas[3].alternativa}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{width: '100%', height: 60}}>
                        <TouchableOpacity style={styleSelected(this.state.actualQuestion.respostas[4].valor)}
                            onPress={() => this.setAnswer(this.state.actualQuestion.idPergunta, this.state.actualQuestion.respostas[4].valor)}>
                            <Text numberOfLines={2} style={textStyleSelected(this.state.actualQuestion.respostas[4].valor)}>{this.state.actualQuestion.respostas[4].alternativa}</Text>
                        </TouchableOpacity>
                    </View>

                    {nextButton}
                    
                </View>
            )
        }
    }

    render()
    {
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                {Platform.OS == 'ios' ? <View style={{position: 'absolute', width: '100%', justifyContent: 'center', alignItems: 'center', top: -770, zIndex: 999}}>
                    <Image source={bkgCircle} style={{resizeMode: 'cover'}} resizeMethod="resize" />
                </View> : null }
                <View style={{ height: 65, width: '100%', alignItems: 'center', position: 'absolute', top: 0, zIndex: 1000}}>
                    <Feather color={(Platform.OS == 'ios') ? '#fff' : '#440276'} style={{ position: 'absolute', padding: 10, left: 5, top: Platform.OS == 'ios' ? 20 : 10, zIndex: 100 }} 
                        size={25} name={'chevron-left'} onPress={() => this.promptToGoBack()}>
                    </Feather>
                    <Text style={{ width: '100%', textAlign: 'center', color: (Platform.OS == 'ios') ? '#fff' : '#440276', fontFamily: 'font-bold', fontSize: 18, marginTop: 35 }}>
                        {(this.state.project) ? Config.getType(this.state.project.type) : null}</Text>
                </View>

                {this.state.actualQuestion ?
                <View style={{width: '100%', height: 140, position: 'absolute', top: 65, zIndex: 1000, alignItems: 'center'}}>
                    <Text style={{color: (Platform.OS == 'ios') ? '#fff' : '#440276', fontSize: 18, padding: 10, paddingTop: 0, textAlign: 'center'}}>{this.state.actualQuestion.descricao}</Text>
                    <Text style={{color: (Platform.OS == 'ios') ? '#fff' : '#440276', fontSize: 14, fontWeight: 'bold'}}>{this.state.actualQuestion.idPergunta} de {this.state.questions.length}</Text>
                </View>
                : null}

                <View style={{width: '100%', marginTop: 205}}>
                    {this.renderQuestion()}
                </View>

            </View>
        )
    }

}