import React from 'react';
import { Platform, Text, View, TouchableOpacity, Keyboard, TouchableWithoutFeedback, ActivityIndicator, Alert, ScrollView, Image, TextInput, AsyncStorage } from 'react-native';
import Config from '../../utils/Config';
import User from '../../models/User';
import ResultModal from './includes/ResultModal';
import Feather from 'react-native-vector-icons/Feather';
import Functions from '../../utils/Functions';
import bkgCircle from '../../assets/images/bkg-circle.png';

export default class RegisterScreen extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { navigation } = this.props;
        this.state = {name: null, email: null, projectName: null, type: navigation.getParam('type'), loading: false};
    }

    componentWillMount()
    {
        // this.checkIfHasOpenedQuiz();
    }

    async checkIfHasOpenedQuiz()
    {
        try{
            var hasOpenedQuiz = await AsyncStorage.getItem('hasOpenedQuiz');
            if(hasOpenedQuiz){
                var slug = await AsyncStorage.getItem('openedQuizProject');
                this.props.navigation.navigate('Quiz', {slug: slug});
            }
        }catch(e){
            console.log(e)
        }
    }

    async createProject()
    {
        if(this.validate()){
            try{
                //Chave do projeto na lista de Projetos no Storage
                var slug = Functions.stringToSlug(this.state.projectName);
                if(!await User.hasProject(slug)){
                    var project = {
                        name: this.state.name,
                        email: this.state.email,
                        projectName: this.state.projectName,
                        slug: slug,
                        type: this.state.type,
                        endedQuiz: false
                    };
                    await User.saveProject(project);
                    this.props.navigation.goBack();
                    this.props.navigation.navigate('Quiz', {slug: slug});
                }else{
                    Alert.alert('Aviso', 'Você já cadastrou um projeto com este nome!');
                }
            }catch(e){
                console.log(e)
                Alert.alert('Aviso', 'Ocorreu um erro ao salvar o projeto!');
                return false;
            }
        }
    }

    validate()
    {
        if(!this.state.name || this.state.name == ''){
            Alert.alert('Aviso', 'Você deve preencher o seu nome.');
            return false;
        }
        if(!this.state.email || this.state.email == ''){
            Alert.alert('Aviso', 'Você deve preencher o seu e-mail.');
            return false;
        }
        if(!Functions.validateEmail(this.state.email)){
            Alert.alert('Aviso', 'E-mail inválido. Confira e tente novamente.');
            return false;
        }
        return true;
    }

    render()
    {
        if(this.state.loading){
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#440276'}}>
                    <ActivityIndicator color={'#fff'} size={'large'}></ActivityIndicator>
                </View>
            )
        }else{
            return (
                <View style={{flex: 1}}>
                    <TouchableWithoutFeedback style={{flex: 1}} onPress={Keyboard.dismiss} accessible={false}>
                        <View style={{flex: 1}}>
                            <View style={{position: 'absolute', width: '100%', justifyContent: 'center', alignItems: 'center', top: -600}}>
                                <Image source={bkgCircle} style={{resizeMode: 'cover'}} />
                            </View>
                            <View style={{ height: 65, width: '100%', alignItems: 'center'}}>
                                <Feather color={'#fff'} style={{ position: 'absolute', padding: 10, left: 5, top: Platform.OS == 'ios' ? 20 : 10, zIndex: 100 }} 
                                    size={25} name={'chevron-left'} onPress={() => this.props.navigation.goBack()}>
                                </Feather>
                                <Text style={{ width: '100%', textAlign: 'center', color: '#fff', fontFamily: 'font-bold', fontSize: 18, marginTop: 35 }}>{Config.getType(this.state.type)}</Text>
                            </View>
            
                            <View style={{flex: 1, width: '100%', alignItems: 'center', justifyContent: 'flex-start', marginTop: 40}}>
            
                                <TextInput
                                    textContentType='none'
                                    value={this.state.name}
                                    placeholder={'Nome'}
                                    placeholderTextColor={'#fff'}
                                    selectionColor={'#fff'}
                                    style={{width: '80%', color: '#fff', borderBottomColor: '#fff', borderBottomWidth: 1, fontSize: 19, height: Platform.OS == 'ios' ? 35 : 40, paddingLeft: 10, 
                                    textAlign: 'left', fontFamily: 'font-regular', marginTop: 30}}
                                    onChangeText={(val) => this.setState({name: val})} 
                                    underlineColorAndroid='transparent' />
            
                                <TextInput autoCapitalize={'none'} 
                                    textContentType='none'
                                    value={this.state.email}
                                    placeholder={'E-mail'}
                                    placeholderTextColor={'#fff'}
                                    selectionColor={'#fff'}
                                    style={{width: '80%', color: '#fff', borderBottomColor: '#fff', borderBottomWidth: 1, fontSize: 19, height: Platform.OS == 'ios' ? 35 : 40, paddingLeft: 10, 
                                    textAlign: 'left', fontFamily: 'font-regular', marginTop: 40}}
                                    keyboardType={'email-address'} onChangeText={(val) => this.setState({email: val})} 
                                    underlineColorAndroid='transparent' />
            
                                <TextInput autoCapitalize={'none'} 
                                    textContentType='none'
                                    value={this.state.projectName}
                                    placeholder={'Projeto'}
                                    placeholderTextColor={'#fff'}
                                    selectionColor={'#fff'}
                                    style={{width: '80%', color: '#fff', borderBottomColor: '#fff', borderBottomWidth: 1, fontSize: 19, height: Platform.OS == 'ios' ? 35 : 40, paddingLeft: 10, 
                                    textAlign: 'left', fontFamily: 'font-regular', marginTop: 40}}
                                    onChangeText={(val) => this.setState({projectName: val})} 
                                    underlineColorAndroid='transparent' />

                                <TouchableOpacity onPress={() => this.createProject()}
                                    style={{width: '60%', height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#FF5000', position: 'absolute', bottom: 40, borderRadius: 5}}>
                                    <Text style={{color: '#fff', fontSize: 22}}>Começar</Text>
                                </TouchableOpacity>
            
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
        }
        
    }

}