import React from 'react';
import { Platform, Text, View, TouchableOpacity, Modal, AsyncStorage, ActivityIndicator, Alert, ScrollView, Image, StatusBar } from 'react-native';
import Config from '../../utils/Config';
import User from '../../models/User';
import Feather from 'react-native-vector-icons/Feather';
import ResultModal from './includes/ResultModal';
import bkgStar from '../../assets/images/bkg-star.png';
import bkgStarLegend from '../../assets/images/bkg-star-legend.png';
import Svg, { Circle, Polygon } from 'react-native-svg';
import { Tooltip } from 'react-native-elements';
import * as Progress from 'react-native-progress';
import individualData from '../../data/individuo';
import startupData from '../../data/startup';
import institucionalData from '../../data/colaborador';

const starMetrics = {
    'p1': {
        'minX': 48.5,
        'minY': 40,
        'maxX': 48.5,
        'maxY': 12
    },
    'p2': {
        'minX': 55,
        'minY': 42.5,
        'maxX': 71.5,
        'maxY': 19
    },
    'p3': {
        'minX': 59.5,
        'minY': 48,
        'maxX': 86,
        'maxY': 39
    },
    'p4': {
        'minX': 59,
        'minY': 54.5,
        'maxX': 85,
        'maxY': 63.5
    },
    'p5': {
        'minX': 55,
        'minY': 60.5,
        'maxX': 71.5,
        'maxY': 83
    },
    'p6': {
        'minX': 48.5,
        'minY': 62,
        'maxX': 48.5,
        'maxY': 91
    },
    'p7': {
        'minX': 42.5,
        'minY': 60.5,
        'maxX': 25.5,
        'maxY': 83
    },
    'p8': {
        'minX': 38,
        'minY': 54.5,
        'maxX': 11,
        'maxY': 63.5
    },
    'p9': {
        'minX': 37.5,
        'minY': 48,
        'maxX': 11,
        'maxY': 39
    },
    'p10': {
        'minX': 42,
        'minY': 42.5,
        'maxX': 25.5,
        'maxY': 19.5
    },
}

const starColors = {
    0: '#ed1c24',
    1: '#f15623',
    2: '#f47e23',
    3: '#f7a922',
    4: '#facc21',
    5: '#fced21',
    6: '#c3d929',
    7: '#89c431',
    8: '#59b238',
    9: '#2ca23f',
    10: '#009245'
}

const starColorsBorders = {
    0: '#c22121',
    1: '#c64b1f',
    2: '#cb6d21',
    3: '#d2911e',
    4: '#d3ae1c',
    5: '#d4c728',
    6: '#a4b32c',
    7: '#78a430',
    8: '#4d9830',
    9: '#298c36',
    10: '#097e3a'
}

export default class ResultScreen extends React.Component {

    constructor(props, context) {
        super(props, context);
        const { navigation } = this.props;
        this.state = {slug: navigation.getParam('slug'), loading: true, media: 0, project: null, data: null, questions: null, answers: null, showResultModal: false, 
        points: {
            'p1': '48.5,12', //X,Y
            'p2': '55,42.5', //X,Y
            'p3': '86,39', //X,Y
            'p4': '59,54.5', //X,Y
            'p5': '71.5,83', //X,Y
            'p6': '48.5,62', //X,Y
            'p7': '25.5,83', //X,Y
            'p8': '38,54.5', //X,Y
            'p9': '11,39', //X,Y
            'p10': '42,42.5' //X,Y
        }, comparisionPoints: {
            'p1': '48.5,12', //X,Y
            'p2': '55,42.5', //X,Y
            'p3': '86,39', //X,Y
            'p4': '59,54.5', //X,Y
            'p5': '71.5,83', //X,Y
            'p6': '48.5,62', //X,Y
            'p7': '25.5,83', //X,Y
            'p8': '38,54.5', //X,Y
            'p9': '11,39', //X,Y
            'p10': '42,42.5' //X,Y
        }, starColor: '#440276', showComparision: false};
    }

    async componentWillMount()
    {
        this.loadProject();
    }

    async loadProject()
    {
        try{
            var project = await User.getProject(this.state.slug);
            if(project){
                var data = {};
                switch(project.type){
                    case 'individual':
                        data = individualData;
                        break;
                    case 'startup':
                        data = startupData;
                        break;
                    case 'institucional':
                        data = institucionalData;
                        break
                }
                var questions = [];
                data.dimensoes.map(d => {
                    d.perguntas.map(p => {
                        questions.push({...p, idDimensao: d.idDimensao});
                    })
                })
                this.setState({project: project, data: data, questions: questions});
                this.calculateMedia();
            }else{
                Alert.alert('Erro', 'Projeto não encontrado.');
            }
        }catch(e){
            console.log(e)
            Alert.alert('Erro', 'Erro ao carregar projeto.');
        }
    }

    calculateMedia()
    {
        if(this.state.project){
            var dimensionsMedia = {};
            this.state.data.dimensoes.map(d => {
                dimensionsMedia[d.idDimensao] = {
                    "total": d.perguntas.length * 5,
                    "nota": 0,
                    "cor": d.cor
                };
            })
            this.state.project.answers.map((answer) => {
                var dimension = this.state.data.dimensoes.filter(d => {
                    return d.idDimensao == answer.idDimensao
                })
                dimensionsMedia[dimension[0].idDimensao].nota += answer.resposta
            })
            var totalGreen = 0;
            var notaFinalGreen = 0;
            var totalRed = 0;
            var notaFinalRed = 0;
            Object.keys(dimensionsMedia).map(idDimensao => {
                if(dimensionsMedia[idDimensao].cor == 'green'){
                    totalGreen += dimensionsMedia[idDimensao].total;
                    notaFinalGreen += dimensionsMedia[idDimensao].nota;
                }else{
                    totalRed += dimensionsMedia[idDimensao].total;
                    notaFinalRed += dimensionsMedia[idDimensao].nota;
                }
            })
            var mediaFinalGreen = (notaFinalGreen * 100) / totalGreen;
            var mediaFinalRed = 100 - (notaFinalRed * 100 / totalRed);
            mediaFinalGreen = mediaFinalGreen <= 20 ? 0 : mediaFinalGreen;
            mediaFinalRed = mediaFinalRed >= 80 ? 100 : mediaFinalRed;

            var mediaFinal = (mediaFinalGreen + mediaFinalRed) / 2;
            this.setState({media: mediaFinal.toFixed(0) / 100, dimensionsMedia: dimensionsMedia});
            this.calculateStarColor(mediaFinal.toFixed(0));
            this.calculateStar(dimensionsMedia);
        }
    }

    calculateStar(dimensionsMedia)
    {
        var arrPoints = {};
        Object.keys(dimensionsMedia).map(idDimensao => {
            var mediaFinalDimensao = (dimensionsMedia[idDimensao].nota * 100 / dimensionsMedia[idDimensao].total).toFixed(0);
            if(mediaFinalDimensao == 20){
                mediaFinalDimensao = 0;
            }

            //Pega valor X mínimo e X máximo, e calcula o valor de intervalo. Ex: minX = 12 maxX = 40 inteval = 40 - 12 = 28
            var minX = starMetrics['p' + idDimensao].minX;
            var maxX = starMetrics['p' + idDimensao].maxX;

            var minY = starMetrics['p' + idDimensao].minY;
            var maxY = starMetrics['p' + idDimensao].maxY;

            var isInvertedX = false;
            var isInvertedY = false;
            if(minX < maxX){
                var intervalX = maxX - minX;
            }else{
                var intervalX = minX - maxX;
                isInvertedX = true;
            }

            //Com o valor do intervalo, faz-se a regra de 3. Sendo o valor máximo do intervalo 100%
            var mediaX = (intervalX * mediaFinalDimensao) / 100;
            //Após calcular, soma-se no valor mínimo, respeitando a regra da vértice (ponta ou base da estrela)
            var finalX = (isInvertedX) ? (minX - mediaX) : (minX + mediaX);

            //Realiza o mesmo processo para Y mínimo e Y máximo
            if(minY < maxY){
                var intervalY = maxY - minY;
            }else{
                var intervalY = minY - maxY;
                isInvertedY = true;
            }

            var mediaY = (intervalY * mediaFinalDimensao) / 100;
            var finalY = (isInvertedY) ? (minY - mediaY) : (minY + mediaY);

            //Atualiza o estado da vértice
            arrPoints["p" + idDimensao] = finalX.toFixed(1) + ',' + finalY.toFixed(1);
        })
        this.setState({points: arrPoints})
    }

    calculateStarColor(media)
    {
        var color = this.state.starColor;
        var colorBorder = this.state.starColorBorder;
        if(media == 0){
            color = starColors[0];
            colorBorder = starColorsBorders[0];
        }else if(media <= 10){
            color = starColors[1];
            colorBorder = starColorsBorders[1];
        }else if(media <= 20){
            color = starColors[2];
            colorBorder = starColorsBorders[2];
        }else if(media <= 30){
            color = starColors[3];
            colorBorder = starColorsBorders[3];
        }else if(media <= 40){
            color = starColors[4];
            colorBorder = starColorsBorders[4];
        }else if(media <= 50){
            color = starColors[5];
            colorBorder = starColorsBorders[5];
        }else if(media <= 60){
            color = starColors[6];
            colorBorder = starColorsBorders[6];
        }else if(media <= 70){
            color = starColors[7];
            colorBorder = starColorsBorders[7];
        }else if(media <= 80){
            color = starColors[8];
            colorBorder = starColorsBorders[8];
        }else if(media <= 90){
            color = starColors[9];
            colorBorder = starColorsBorders[9];
        }else if(media <= 100){
            color = starColors[10];
            colorBorder = starColorsBorders[10];
        }
        this.setState({starColor: color, starColorBorder: colorBorder});
    }


    render()
    {
        var points = this.state.points;
        var pointsString = points.p1 + ' ' + points.p2 + ' ' + points.p3 + ' ' + points.p4 + ' ' + points.p5 + ' ' 
            + points.p6 + ' ' + points.p7 + ' ' + points.p8 + ' ' + points.p9 + ' ' + points.p10;

        var comparisionPoints = this.state.comparisionPoints;
        var comparisionPointsString = comparisionPoints.p1 + ' ' + comparisionPoints.p2 + ' ' + comparisionPoints.p3 + ' ' + comparisionPoints.p4 + ' ' + comparisionPoints.p5 + ' ' 
        + comparisionPoints.p6 + ' ' + comparisionPoints.p7 + ' ' + comparisionPoints.p8 + ' ' + comparisionPoints.p9 + ' ' + comparisionPoints.p10;
        
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <View style={{ height: 65, width: '100%', alignItems: 'center', position: 'absolute', top: 0, zIndex: 1000}}>
                    <Feather color={'#440276'} style={{ position: 'absolute', padding: 10, left: 5, top: Platform.OS == 'ios' ? 20 : 10, zIndex: 100 }} 
                        size={25} name={'chevron-left'} onPress={() => this.props.navigation.goBack()}>
                    </Feather>
                    <Text style={{ width: '100%', textAlign: 'center', color: '#440276', fontFamily: 'font-bold', fontSize: 18, marginTop: 35 }}>ANÁLISE VISUAL</Text>
                </View>

                <View style={{flex: 1, marginTop: 65, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{flex: 1, width: '100%', paddingLeft: 20, paddingRight: 20, marginTop: 20}}>
                        <Image style={{width: '100%', height: 350, resizeMode: 'contain', position: 'absolute', alignSelf: 'center', zIndex: 999}} 
                            source={bkgStarLegend}></Image>

                        <View style={{width: '100%', height: 350, position: 'absolute', alignSelf: 'center', zIndex: 998}}>
                            <Svg height="100%" width="100%" viewBox="0 0 100 100">
                                <Circle
                                    cx="48.5"
                                    cy="51.3"
                                    r="40.5"
                                    stroke="#000"
                                    strokeWidth="0.1"
                                    fill="#fff"
                                />
                                { this.state.showComparision ?
                                <Polygon
                                    x="0"
                                    y="0"
                                    points={comparisionPointsString}
                                    fill={'#09a50f'}
                                    stroke={'#09a50f'}
                                    strokeWidth="0.5"
                                /> : null }
                                <Polygon
                                    x="0"
                                    y="0"
                                    points={pointsString}
                                    fill={this.state.starColor}
                                    fillOpacity={this.state.showComparision ? 0.5 : 1}
                                    stroke={this.state.starColorBorder}
                                    strokeWidth="0.5"
                                />
                            </Svg>
                        </View>

                        <View style={{width: '100%', height: 80, position: 'absolute', bottom: 120, 
                            alignSelf: 'center', paddingLeft: 20, paddingRight: 20, alignItems: 'center', justifyContent: 'center'}}>
                            <Tooltip backgroundColor={'#FF5000'} popover={<Text style={{color: '#fff'}}>{(this.state.media * 100).toFixed(0)}%</Text>}>
                                <View>
                                    <Progress.Bar progress={this.state.media} width={200} color={'#ff5000'} unfilledColor={'#440276'} borderWidth={0} />
                                    <View style={{width: 200, flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                                        <Text style={{color: '#440276'}}>0</Text>
                                        <Text style={{color: '#440276'}}>100</Text>
                                    </View>
                                </View>
                            </Tooltip>
                        </View>
                    </View>

                    <View style={{position: 'absolute', width: '100%', bottom: 20}}>
                        <TouchableOpacity style={{width: '60%', alignSelf: 'center', alignItems: 'center', 
                            justifyContent: 'center', backgroundColor: '#ff5000', marginTop: 30, borderRadius: 5}} 
                            onPress={() => this.setState({showComparision: this.state.showComparision ? false : true})}>
                            <Text style={{fontSize: 20, color: '#fff', padding: 10}}>Comparação</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{width: '60%', alignSelf: 'center', alignItems: 'center', 
                            justifyContent: 'center', backgroundColor: '#ff5000', marginTop: 10, borderRadius: 5}} 
                            onPress={() => this.setState({showResultModal: true})}>
                            <Text style={{fontSize: 20, color: '#fff', padding: 10}}>Diagnóstico</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <ResultModal points={this.state.points} comparisionPoints={this.state.comparisionPoints} 
                    media={this.state.media} starColor={this.state.starColor}
                    dimensionsMedia={this.state.dimensionsMedia} project={this.state.project} show={this.state.showResultModal} 
                    close={() => this.setState({showResultModal: false})}></ResultModal>

            </View>
        )
    }

}