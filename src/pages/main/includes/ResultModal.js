import React from 'react';
import { Platform, Text, View, TouchableOpacity, Modal, Slider, Alert, ScrollView, Image } from 'react-native';
import Config from '../../../utils/Config';
import User from '../../../models/User';
import { captureRef } from "react-native-view-shot";
import Ionicons from 'react-native-vector-icons/Ionicons';
import bkgDiagnosis from '../../../assets/images/orientation.png';
import individualData from '../../../data/individuo';
import startupData from '../../../data/startup';
import institucionalData from '../../../data/colaborador';
import dataDiagnosis from '../../../data/diagnosis.json';
import bkgStarLegend from '../../../assets/images/bkg-star-legend.png';
import Svg, { Circle, Polygon } from 'react-native-svg';
import { Tooltip } from 'react-native-elements';
import * as Progress from 'react-native-progress';
import * as Sharing from 'expo-sharing';

export default class ResultModal extends React.Component {

    constructor(props, context)
    {
        super(props, context);

        this.state = { };
    }

    pdfContainer()
    {
        var points = this.props.points;
        var pointsString = points.p1 + ' ' + points.p2 + ' ' + points.p3 + ' ' + points.p4 + ' ' + points.p5 + ' ' 
            + points.p6 + ' ' + points.p7 + ' ' + points.p8 + ' ' + points.p9 + ' ' + points.p10;

        var comparisionPoints = this.props.comparisionPoints;
        var comparisionPointsString = comparisionPoints.p1 + ' ' + comparisionPoints.p2 + ' ' + comparisionPoints.p3 + ' ' + comparisionPoints.p4 + ' ' + comparisionPoints.p5 + ' ' 
        + comparisionPoints.p6 + ' ' + comparisionPoints.p7 + ' ' + comparisionPoints.p8 + ' ' + comparisionPoints.p9 + ' ' + comparisionPoints.p10;
        return ( 
            <View ref={(component) => this.pdfContainerRef = component} style={{flex: 1, width: '100%', minHeight: 2100, paddingLeft: 20, paddingRight: 20, marginTop: 20}}>

                <View style={{width: '100%', paddingLeft: 20, height: 420, paddingRight: 20, marginTop: 20}}>
                    <Image style={{width: '100%', height: 350, resizeMode: 'contain', position: 'absolute', alignSelf: 'center', zIndex: 999}} 
                        source={bkgStarLegend}></Image>

                    <View style={{width: '100%', height: 350, position: 'absolute', alignSelf: 'center', zIndex: 998}}>
                        <Svg height="100%" width="100%" viewBox="0 0 100 100">
                            <Circle
                                cx="48.5"
                                cy="51.3"
                                r="40.5"
                                stroke="#000"
                                strokeWidth="0.1"
                                fill="#fff"
                            />
                            <Polygon
                                x="0"
                                y="0"
                                points={comparisionPointsString}
                                fill={'#09a50f'}
                                stroke={'#09a50f'}
                                strokeWidth="0.5"
                            />
                            <Polygon
                                x="0"
                                y="0"
                                points={pointsString}
                                fill={this.props.starColor}
                                fillOpacity={0.5}
                                stroke={this.props.starColor}
                                strokeWidth="0.5"
                            />
                        </Svg>
                    </View>

                    <View style={{width: '100%', height: 80, position: 'absolute', top: 360, 
                        alignSelf: 'center', paddingLeft: 20, paddingRight: 20, alignItems: 'center', justifyContent: 'center'}}>
                        <Tooltip backgroundColor={'#FF5000'} popover={<Text style={{color: '#fff'}}>{this.props.media * 100}%</Text>}>
                            <View>
                                <Progress.Bar progress={this.props.media} width={200} color={'#ff5000'} unfilledColor={'#440276'} borderWidth={0} />
                                <View style={{width: 200, flexDirection: 'row', justifyContent: 'space-between', marginTop: 5}}>
                                    <Text style={{color: '#440276'}}>0</Text>
                                    <Text style={{color: '#440276'}}>100</Text>
                                </View>
                            </View>
                        </Tooltip>
                    </View>
                </View>
                
                <View style={{width: '100%', minHeight: 400, paddingBottom: 40}}>

                    {this.renderDiagnosis()}

                </View>
            </View>
        )
    }

    async generatePDF()
    {   
        captureRef(this.pdfContainerRef, {format: 'jpg', quality: 1}).then(uri => Sharing.shareAsync('file://' + uri), error => console.error(error));
    }

    renderDiagnosis()
    {
        if(this.props.project && this.props.dimensionsMedia){
            var diagnosticos = null;
            var data = null;
            switch(this.props.project.type){
                case 'individual':
                    diagnosticos = dataDiagnosis.diagnosticos.individuo.dimensoes;
                    data = individualData.dimensoes;
                    break;
                case 'institucional':
                    diagnosticos = dataDiagnosis.diagnosticos.colaborador.dimensoes;
                    data = institucionalData.dimensoes;
                    break;
                case 'startup':
                    diagnosticos = dataDiagnosis.diagnosticos.startup.dimensoes;
                    data = startupData.dimensoes;
                    break;
            }
            return Object.keys(diagnosticos).map(k => {
                var dimensao = data.filter(d => {
                    return d.idDimensao == k;
                })
                var title = dimensao[0].nome;
                var mediaFinal = (this.props.dimensionsMedia[k].nota * 100) / this.props.dimensionsMedia[k].total;
                mediaFinal = mediaFinal.toFixed(0);
                var resposta = null;
                
                if(mediaFinal < 20){
                    resposta = diagnosticos[k][20];
                }else if(mediaFinal < 40){
                    resposta = diagnosticos[k][40];
                }else if(mediaFinal < 60){
                    resposta = diagnosticos[k][60];
                }else if(mediaFinal < 80){
                    resposta = diagnosticos[k][80];
                }else{
                    resposta = diagnosticos[k][100];
                }
                
                return (
                    <View style={{width: '100%', marginTop: 30, alignItems: 'center', justifyContent: 'center'}} key={k}>
                        <Text style={{fontWeight: 'bold', fontSize: 18, color: '#555', textAlign: 'center'}}>{title}</Text>
                        <Text style={{fontSize: 16, color: '#000', textAlign: 'justify', marginTop: 10}}>{resposta}</Text>
                    </View>
                )
            })
        }
    }

    render()
    {
        return (
            <Modal visible={this.props.show}  onDismiss={() => this.props.close()} animationType="slide">
                <View style={{flex: 1}}>
                    <View style={{width: '100%', height: 65, position: 'absolute', top: 10, backgroundColor: '##fff', zIndex: 1000}}>
                        <Text style={{ width: '100%', textAlign: 'center', color: '#440276', fontFamily: 'font-bold', fontSize: 18, marginTop: 35 }}>DIAGNÓSTICO</Text>
                        <TouchableOpacity style={{position: 'absolute', top: 25, right: 5, paddingLeft: 20, paddingRight: 20}} onPress={() => this.props.close()}>
                            <Ionicons name={'ios-close'} color={'#440276'} size={30}/>
                        </TouchableOpacity>
                    </View>

                    <ScrollView style={{flex: 1, width: '100%', marginTop: 65}}>
                        <View style={{flex: 1, width: '100%', paddingLeft: 20, paddingRight: 20, marginTop: 20}}>
                            <Image style={{width: '100%', height: 150, resizeMode: 'contain', position: 'absolute', alignSelf: 'center', zIndex: 999}} source={bkgDiagnosis}></Image>
                            
                            <View style={{flex: 1, marginTop: 150, width: '100%', minHeight: 400, paddingBottom: 40}}>

                                {this.renderDiagnosis()}

                                <TouchableOpacity style={{width: '60%', alignSelf: 'center', alignItems: 'center', 
                                    justifyContent: 'center', backgroundColor: '#ff5000', marginTop: 20, borderRadius: 5}} 
                                    onPress={() => this.generatePDF()}>
                                    <Text style={{fontSize: 20, color: '#fff', padding: 10}}>Exportar</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View>
                    {this.pdfContainer()}
                </View>
            </Modal>
        )
    }

}