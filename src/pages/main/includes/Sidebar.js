import React from 'react';
import { Platform, Text, View, TouchableOpacity, Alert, Image } from 'react-native';
import Modal from 'react-native-modal';
import User from '../../../models/User';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Sidebar extends React.Component 
{
    constructor(props, context) {
        super(props, context);
    }

    clearProjects()
    {
        Alert.alert(
            'Apagar Projetos',
            'Deseja realmente apagar os projetos cadastrados?',
            [
              {text: 'Não', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'Sim', onPress: () => {
                  User.deleteProjects();
                  this.props.reloadProjects();
                  Alert.alert('Ação Concluída', 'Todos os seus projetos foram apagados.');
              }},
            ],
            { cancelable: false }
          )
    }

    goToRoute(route)
    {
        this.props.close();
        this.props.navigation.navigate(route)
    }

    render()
    {
        return (
            <Modal isVisible={this.props.show} 
                onBackdropPress={() => this.props.close()}
                onSwipeComplete={() => this.props.close()}
                animationIn={'slideInLeft'} 
                animationOut={'slideOutLeft'} 
                style={{left: '-20%', bottom: 0, margin: 0}}
                swipeDirection="left">
                <View style={{ flex: 1, backgroundColor: '#440276' }}>
                    <TouchableOpacity style={{position: 'absolute', top: 10, right: 10, zIndex: 999, padding: 10}} onPress={() => this.props.close()}>
                        <Ionicons name={'ios-close'} size={30} color={'#fff'} />
                    </TouchableOpacity>

                    <View style={{flex: 1, marginTop: 60, marginLeft: '20%'}}>
                        
                        <TouchableOpacity onPress={() => this.goToRoute('About')} style={{padding: 20, alignItems: 'flex-start', justifyContent: 'center'}}>
                            <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 22}}>Termos de Uso</Text>
                        </TouchableOpacity>

                        {/* <TouchableOpacity onPress={() => this.goToRoute('Book')} style={{padding: 20, alignItems: 'left', justifyContent: 'center'}}>
                            <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 22}}>Livro</Text>
                        </TouchableOpacity> */}

                        <TouchableOpacity onPress={() => this.clearProjects()} style={{padding: 20, alignItems: 'flex-start', justifyContent: 'center'}}>
                            <Text style={{color: '#fff', fontWeight: 'bold', fontSize: 22}}>Apagar Projetos</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>
        )
    }
}