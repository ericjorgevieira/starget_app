import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, StatusBar, Platform, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Swiper from 'react-native-swiper';

import Config from '../../utils/Config';
import slide1 from '../../assets/images/phone-01.png'
import slide2 from '../../assets/images/phone-02.png'
import slide3 from '../../assets/images/phone-03.png'

var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    wrapper: {
        
    },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    }
});

export default class WalkthroughScreen extends React.Component {
    swiper: Object;

    constructor(props, context) {
        super(props, context);
        this.state = {slide: 0}
    }

    static navigationOptions = {
        header: null
    }

    renderHeader()
    {
        StatusBar.setBarStyle('light-content');
        Platform.OS == 'android' && StatusBar.setBackgroundColor('#6a51ae');
        return (
            <View style={{ height: 65, width: '100%', backgroundColor: '#000', position: 'absolute', top: 0, left: 0, right: 0, alignItems: 'center', overflow: 'hidden', zIndex: 9999 }}>
                {this.renderSlideBack()}
                <Text style={{textAlign: 'center', color: '#fff', marginTop: 35, fontSize: 14, fontFamily: 'font-bold'}}>
                    BEM-VINDO
                </Text>
                {this.renderSlideNext()}
                {this.renderSlideDone()}
            </View>
        )
    }

    onSkipBtnHandle = (index) => 
    {
        console.log(index);
        this.props.navigation.navigate('Main');
    }

    doneBtnHandle = () => 
    {
        this.props.navigation.navigate('Main');
    }

    nextBtnHandle = (index) => 
    {
        console.log(index);
    }

    onSlideChangeHandle = (index, total) => 
    {
        console.log(index, total);
    }

    renderSlideBack()
    {
        if(this.state.slide > 0){
            return (
                <Ionicons color={'#fff'} style={{ position: 'absolute', padding: 10, left: 5, top: 20, zIndex: 100 }} size={25} name={'ios-arrow-back'} onPress={() => this.handleSlideBack()}>
                </Ionicons>
            )
        }
    }

    renderSlideNext()
    {
        if(this.state.slide < 2){
            return (
                <TouchableOpacity style={{position: 'absolute', right: 10, top: 0, zIndex: 200}} onPress={() => this.handleSlideNext()}>
                    <Text style={{ color: '#fff', marginTop: 35, fontSize: 14}}>
                        Próximo
                    </Text>
                </TouchableOpacity>
            )
        }
    }

    renderSlideDone()
    {
        if(this.state.slide == 2){
            return (
                <TouchableOpacity style={{position: 'absolute', right: 10, top: 0, zIndex: 200, padding: 5}} onPress={() => this.props.navigation.navigate('Main')}>
                    <Text style={{ color: '#fff', marginTop: 35, fontSize: 14}}>
                        OK
                    </Text>
                </TouchableOpacity>
            )
        }
    }

    handleSlideBack() 
    {
        if(this.state.slide > 0){
            this.swiper.scrollBy(-1, true)
        }
    }

    handleSlideNext() 
    {
        if(this.state.slide < 2){
            this.swiper.scrollBy(1, true)
        }
    }

    render() 
    {
        return (
            <View style={{flex:1}}>
                {this.renderHeader()}
                <Swiper loop={false} ref={(ref) => {this.swiper = ref}} onIndexChanged={(i) => this.setState({slide: i})}
                    showButtons={false} style={styles.wrapper} activeDotColor={'#000'} showsPagination={width < 321 ? false : true}>
                    <View style={styles.slide1}>
                        <Image source={slide1} style={{flex: 1, width: '100%', resizeMode: 'cover'}} />
                        <View style={{flex: 1, position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#333', height: '20%'}}>
                            <Text style={{textAlign: 'center', fontSize: 16, fontFamily: 'font-bold', marginTop: 20, color: '#000'}}>{Config.FIRST_STEP_TITLE}</Text>
                            <Text style={{textAlign: 'center', fontSize: 14, fontFamily: 'font-regular', marginTop: 10, paddingLeft: 20, paddingRight: 20, color: '#fff'}}>
                                {Config.FIRST_STEP_TEXT}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.slide2}>
                        <Image source={slide2} style={{flex: 1, width: '100%', resizeMode: 'cover'}} />
                        <View style={{flex: 1, position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#333', height: '20%'}}>
                            <Text style={{textAlign: 'center', fontSize: 16, fontFamily: 'font-bold', marginTop: 20, color: '#000'}}>{Config.SECOND_STEP_TITLE}</Text>
                            <Text style={{textAlign: 'center', fontSize: 14, fontFamily: 'font-regular', marginTop: 10, paddingLeft: 20, paddingRight: 20, color: '#fff'}}>
                                {Config.SECOND_STEP_TEXT}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.slide3}>
                        <Image source={slide3} style={{flex: 1, width: '100%', resizeMode: 'cover'}} />
                        <View style={{flex: 1, position: 'absolute', bottom: 0, width: '100%', backgroundColor: '#333', height: '20%'}}>
                            <Text style={{textAlign: 'center', fontSize: 16, fontFamily: 'font-bold', marginTop: 20, color: '#000'}}>{Config.THIRD_STEP_TITLE}</Text>
                            <Text style={{textAlign: 'center', fontSize: 14, fontFamily: 'font-regular', marginTop: 10, paddingLeft: 20, paddingRight: 20, color: '#fff'}}>
                                {Config.THIRD_STEP_TEXT}
                            </Text>
                        </View>
                    </View>
                </Swiper>
            </View>
        );
    }
}