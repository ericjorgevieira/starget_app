import React from 'react';
import { BackHandler, View, ScrollView, Text, StatusBar, Platform, ActivityIndicator } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import User from '../../models/User';

export default class BookScreen extends React.Component {
    _didFocusSubscription;
    _willBlurSubscription;

    constructor(props, context) {
        super(props, context);
        this.state = {showTerms: true}
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        StatusBar.setBarStyle('light-content');
    }

    onBackButtonPressAndroid = () => {
        return true;
    };

    componentDidMount(){
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }

    componentWillUnmount(){
        this._didFocusSubscription && this._didFocusSubscription.remove();
        this._willBlurSubscription && this._willBlurSubscription.remove();
    }

    goToMain()
    {
        this.props.navigation.goBack();
    }

    render() {
        if(this.state.showTerms){
            return (
                <View style={{flex: 1}}>
                    <View style={{ height: 65, width: '100%', alignItems: 'center', backgroundColor: '#fff'}}>
                        <Feather color={'#440276'} style={{ position: 'absolute', padding: 10, left: 5, top: Platform.OS == 'ios' ? 20 : 10, zIndex: 100 }} 
                            size={25} name={'chevron-left'} onPress={() => this.goToMain()}>
                        </Feather>
                        <Text style={{ width: '100%', textAlign: 'center', color: '#440276', fontFamily: 'font-bold', fontSize: 18, marginTop: 35 }}>LIVRO</Text>
                    </View>
                    <View style={{flex: 1, backgroundColor: '#fff'}}>
                        <ScrollView style={{flex: 1, padding: 20}}>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 24, textAlign: 'center', marginTop: 10}}>
                                Termos de Uso e Política de Privacidade
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                1. DOS OBJETIVOS
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Trata-se de uma plataforma digital que auxilia o trabalho de gestores e otimiza a regência de alocação de plantonistas na área da saúde. 
                                Salienta-se que, é de responsabilidade do usuário ter conhecimento dos próprios plantões.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                2. DO CADASTRO
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma só poderá ser acessada por usuário plenamente capaz, de acordo com a legislação brasileira. 
                                Quanto ao usuário que for menor de idade ou declarado incapaz em quaisquer aspectos, deve estar devidamente assistido por seu representante legal. 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Após o usuário enviar um endereço de e-mail válido e receber a confirmação do cadastro, o administrador do grupo dará um código de acesso. 
                                O uso desse código é de responsabilidade do usuário e não poderá ser informado a terceiros.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                3. DAS RESPONSABILIDADES
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A platafoma MedScale se responsabilizará pelos defeitos ou vícios encontrados nos serviços prestados, desde que tenha lhes dado causa. 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma não assegura que esteja livre de perda, interrupção, ataque, vírus, interferência, pirataria ou outra invasão de segurança e 
                                isenta-se de qualquer responsabilidade em relação a essas questões. O usuário é responsável pelo backup do seu próprio dispositivo, 
                                bem como pelo cumprimento das regras contidas no presente termo e pela proteção dos dados de acesso ao aplicativo.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                4. DOS DIREITOS AUTORAIS
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Toda estrutura da plataforma MedScale, gráficos, imagens e demais aplicações são de propriedade do editor e são 
                                protegidas pelas leis referentes à propriedade intelectual. 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                É vedado ao usuário incluir na plataforma informações que possam modificar o seu conteúdo ou sua aparência.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                5. DA POLÍTICA DE PRIVACIDADE
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.1. Informações Gerais
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A presente Política de Privacidade contém informações a respeito do modo como tratamos, total ou parcialmente, de 
                                forma automatizada ou não, os dados pessoais dos usuários que acessam nossa plataforma.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.2. Direitos do Usuário
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma MedScale se compromete a cumprir as normas previstas no Regulamento Geral sobre a Proteção de Dados (RGPD) 
                                e na Lei de Proteção de Dados Pessoais. 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O usuário poderá exercer os seus direitos por meio de comunicação escrita enviada a plataforma com o assunto "RGDP-", especificando: 
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                - Nome completo ou razão social, número do CPF (Cadastro de Pessoas Físicas, da Receita Federal do Brasil) ou 
                                    CNPJ (Cadastro Nacional de Pessoa Jurídica, da Receita Federal do Brasil) e endereço de e-mail do usuário e, se for o caso, do seu representante;{"\n"}
                                - Direito que deseja exercer junto a plataforma;{"\n"}
                                - Data do pedido e assinatura do usuário;{"\n"}
                                - Todo documento que possa demonstrar ou justificar o exercício de seu direito.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O pedido deverá ser enviado ao e-mail: medscale.app@gmail.com, ou por correio, ao seguinte endereço:
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                MEDSCALE PLATFORMA INTEGRADA{"\n"}
                                Rua das Embarcações, número 50 - Residencial Jangadas, bloco 25, apartamento 408, Nova Parnamirim. CEP: 59.152-822.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O usuário será informado em caso de retificação ou eliminação dos seus dados.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.3. Dos dados coletados
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.3.1. Dados de identificação do usuário para realização de cadastro
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A utilização, pelo usuário, de determinadas funcionalidades da plataforma dependerá de cadastro, sendo que, nestes casos,
                                os seguintes dados do usuário serão coletados e armazenados:
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                - nome completo{"\n"}
                                - data de nascimento{"\n"}
                                - endereço de e-mail{"\n"}
                                - número de telefone{"\n"}
                                - número de CPF{"\n"}
                                - número de CNPJ{"\n"}
                                - dados relativos à profissão (ex: CRM)
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.3.2. Dados relacionados à execução de contratos firmados com o usuário
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Para a execução de contrato de compra e venda ou de prestação de serviços eventualmente firmado entre a plataforma e o usuário,
                                poderão ser coletados e armazenados outros dados relacionados ou necessários a sua execução, incluindo o teor de eventuais comunicações
                                tidas com o usuário.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.4. Registros de acesso
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Em atendimento às disposições do artigo 15, caput e parágrafos, da Lei Federal n. 12.965/2014 (Marco Civil da Internet), 
                                os registros de acesso do usuário serão coletados e armazenados por, pelo menos, seis meses.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.5. Fundamento jurídico para o tratamento dos dados pessoais
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Ao utilizar os serviços da plataforma, o usuário está consentindo com a presente Política de Privacidade.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O usuário tem o direito de retirar seu consentimento a qualquer momento, 
                                não comprometendo a licitude do tratamento de seus dados pessoais antes da retirada. 
                                A retirada do assentimento poderá ser feita pelo e-mail: medscale.app@gmail.com, ou por correio enviado ao seguinte endereço:
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Rua das Embarcações, número 50 - Residencial Jangadas, bloco 25, apartamento 408, Nova Parnamirim. CEP: 59.152-822.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Poderão ainda ser coletados dados pessoais necessários para a execução e cumprimento dos serviços contratados pelo usuário na plaforma.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O tratamento de dados pessoais sem o consentimento do usuário apenas será realizado em razão de interesse legítimo ou para as hipóteses previstas em lei.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.6. Finalidades do tratamento dos dados pessoais
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Os dados pessoais do usuário coletados pela plataforma MedScale têm por finalidade facilitar, agilizar e cumprir os compromissos
                                estabelecidos com o usuário e a fazer cumprir as solicitações realizadas por meio do preenchimento de formulários.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma recolhe os dados do usuário para que seja realizada definição de perfis (profiling), ou seja, 
                                tratamento automatizado de dados pessoais que consista em utilizar estes dados para avaliar características relacionadas ao seu desempenho profissional.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O tratamento de dados pessoais para finalidades não previstas nesta Política de Privacidade somente ocorrerá mediante comunicação prévia ao usuário, 
                                sendo que, em qualquer caso, os direitos e obrigações aqui previstos permanecerão aplicáveis.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.7. Prazo de conservação dos dados pessoais
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Os dados pessoais do usuário serão conservados pelo prazo que conste em seu contrato, exceto se o usuário solicitar a 
                                sua supressão antes do final deste prazo.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Os dados pessoais dos usuários apenas poderão ser conservados após o término de seu tratamento nas seguintes hipóteses:
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                - para o cumprimento de obrigação legal ou regulatória pelo controlador;{"\n"}
                                - para estudo por órgão de pesquisa, garantida, sempre que possível, a anonimização dos dados pessoais;{"\n"}
                                - para a transferência a terceiro, desde que respeitados os requisitos de tratamento de dados dispostos na legislação;{"\n"}
                                - para uso exclusivo do controlador, vedado seu acesso por terceiro, e desde que anonimizados os dados.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.8. Destinatários e transferência dos dados pessoais
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Os dados pessoais do usuário não serão compartilhados com terceiros. Serão, portanto, tratados apenas por esta plataforma.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.9. Do tratamento dos dados pessoais
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.9.1 Do responsável pelo tratamento dos dados (data controller) e da proteção dos dados (data protection officer)
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Nesta plataforma, o responsável pelo tratamento dos dados pessoais coletados é MedScale Plataforma Integrada, 
                                representada por Vanessa da Escóssia Pegado Silva, que poderá ser contactada pelo e-mail medscale.app@gmail.com ou no 
                                endereço: Rua das Embarcações, número 50 - Residencial Jangadas, bloco 25, apartamento 408, Nova Parnamirim. CEP: 59.152-822.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma possui também o seguinte responsável pelo tratamento e proteção dos dados pessoais coletados:
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Eric Jorge Vieira de Oliveira, que poderá ser contactado pelo e-mail: ericjorgevieira@gmail.com.
                            </Text>
                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 16, textAlign: 'left', marginTop: 10}}>
                                5.10. Segurança no tratamento dos dados pessoais do usuário
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A plataforma se compromete a aplicar as medidas técnicas e organizativas aptas a proteger os dados pessoais 
                                de acessos não autorizados e de situações de destruição, perda, alteração, comunicação ou difusão de tais dados.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O MedScale utiliza certificado SSL (Secure Socket Layer) que garante que os dados pessoais se transmitam de 
                                forma segura e confidencial, de maneira que a transmissão dos dados entre o servidor e o usuário, e em retroalimentação, 
                                ocorra de maneira totalmente cifrada ou encriptada.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                No entanto, a plataforma se exime de responsabilidade por culpa exclusiva de terceiro, como em caso de ataque de hackers ou crackers, 
                                ou culpa exclusiva do usuário, como no caso em que ele mesmo transfere seus dados a terceiro.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Por fim, a plataforma MedScale se compromete a tratar os dados pessoais do usuário com confidencialidade, dentro dos limites legais.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                6. DAS ALTERAÇÕES
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                A presente versão desta Política de Privacidade foi atualizada pela última vez em: 16/06/2019.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O editor se reserva o direito de modificar, a qualquer momento a plataforma as presentes normas, 
                                especialmente para adaptá-las às evoluções da plataforma MedScale.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O usuário será explicitamente notificado em caso de alteração desta política.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Após publicadas tais alterações, ao continuar com o uso da plataforma você terá aceitado e concordado em cumprir os termos modificados. 
                                Caso discorde de alguma das modificações, deverá pedir, imediatamente, o cancelamento de sua conta e apresentar a sua ressalva ao serviço de atendimento, 
                                se assim o desejar.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                O MedScale pode, de tempos em tempos, modificar ou descontinuar (temporária ou permanentemente) a distribuição ou a atualização desta plataforma.
                            </Text>

                            <Text style={{fontFamily: 'font-bold', color: '#000', fontSize: 18, textAlign: 'left', marginTop: 30}}>
                                7. DO DIREITO APLICÁVEL E DO FORO
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10}}>
                                Para a solução das controvérsias decorrentes do presente instrumento, será aplicado integralmente o Direito brasileiro.
                            </Text>
                            <Text style={{fontFamily: 'font-regular', color: '#000', fontSize: 16, textAlign: 'justify', marginTop: 10, paddingBottom: 40}}>
                                Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede do editor da plataforma.
                            </Text>

                        </ScrollView>
                    </View>
                </View>
            );
        }else{
            return (
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#440276'}}>
                    <ActivityIndicator color={'#fff'} size={'large'}></ActivityIndicator>
                </View>
            )
        }
    }
}