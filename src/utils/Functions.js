function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  
  function arrayShuffle(array){
      var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
  }
  
  function xmlToJson(xml){
      
      var obj = {};
  
      if (xml.nodeType == 1) { // element
          // do attributes
          if (xml.attributes.length > 0) {
          obj["@attributes"] = {};
              for (var j = 0; j < xml.attributes.length; j++) {
                  var attribute = xml.attributes.item(j);
                  obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
              }
          }
      } else if (xml.nodeType == 3) { // text
          obj = xml.nodeValue;
      }
  
      // do children
      // If just one text node inside
      if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
          obj = xml.childNodes[0].nodeValue;
      }
      else if (xml.hasChildNodes()) {
          for(var i = 0; i < xml.childNodes.length; i++) {
              var item = xml.childNodes.item(i);
              var nodeName = item.nodeName;
              if (typeof(obj[nodeName]) == "undefined") {
                  obj[nodeName] = xmlToJson(item);
              } else {
                  if (typeof(obj[nodeName].push) == "undefined") {
                      var old = obj[nodeName];
                      obj[nodeName] = [];
                      obj[nodeName].push(old);
                  }
                  obj[nodeName].push(xmlToJson(item));
              }
          }
      }
      return obj;
  }
  
  function timeDifference(previous){
      if(previous.toString().length < 13){
          var current = Math.round(new Date().getTime() / 1000);
      }else{
          var current = Math.round(new Date().getTime());
      }
  
      var msPerMinute = 60;
      var msPerHour = msPerMinute * 60;
      var msPerDay = msPerHour * 24;
      var msPerMonth = msPerDay * 30;
      var msPerYear = msPerDay * 365;
      var elapsed = current - parseInt(previous);
      
      if(previous.toString().length == 13){
          elapsed = elapsed / 1000;
      }
  
      if (elapsed < msPerMinute) {
           return Math.round(elapsed/1000) + ' segundos atrás';   
      }
  
      else if (elapsed < msPerHour) {
           return Math.round(elapsed/msPerMinute) + ' minutos atrás';   
      }
  
      else if (elapsed < msPerDay ) {
           return Math.round(elapsed/msPerHour ) + ' horas atrás';   
      }
  
      else if (elapsed < msPerMonth) {
          return 'aproximadamente ' + Math.round(elapsed/msPerDay) + ' dias atrás';   
      }
  
      else if (elapsed < msPerYear) {
          return 'aproximadamente ' + Math.round(elapsed/msPerMonth) + ' meses atrás';   
      }
  
      else {
          return 'aproximadamente ' + Math.round(elapsed/msPerYear ) + ' anos atrás';   
      }
  }
  
  function numberToReal(numero, showSymbol = true) {
      var numero = numero.toFixed(2).split('.');
      if(showSymbol){
          numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
      }else{
          numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
      }
      return numero.join(',');
  }
  
  function getOdsList(){
      return [
          {
              number: 1,
              title: 'Erradicação da Pobreza',
              slug: 'erradicacao-da-pobreza',
              url: 'https://nacoesunidas.org/pos2015/ods1/'
          },
          {
              number: 2,
              title: 'Fome Zero e Agricultura Sustentável',
              slug: 'fome-zero',
              url: 'https://nacoesunidas.org/pos2015/ods2/'
          },
          {
              number: 3,
              title: 'Saúde e Bem Estar',
              slug: 'saude-e-bem-estar',
              url: 'https://nacoesunidas.org/pos2015/ods3/'
          },
          {
              number: 4,
              title: 'Educação de Qualidade',
              slug: 'educacao-de-qualidade',
              url: 'https://nacoesunidas.org/pos2015/ods4/'
          },
          {
              number: 5,
              title: 'Igualdade de Gênero',
              slug: 'igualdade-de-genero',
              url: 'https://nacoesunidas.org/pos2015/ods5/'
          },
          {
              number: 6,
              title: 'Água Potável e Saneamento',
              slug: 'agua-potavel-e-saneamento',
              url: 'https://nacoesunidas.org/pos2015/ods6/'
          },
          {
              number: 7,
              title: 'Energia Limpa e Acessível',
              slug: 'energia-limpa-e-acessivel',
              url: 'https://nacoesunidas.org/pos2015/ods7/'
          },
          {
              number: 8,
              title: 'Trabalho Decente e Crescimento Econômico',
              slug: 'trabalho-decente-e-crescimento-economico',
              url: 'https://nacoesunidas.org/pos2015/ods8/'
          },
          {
              number: 9,
              title: 'Indústria, Inovação e Infraestrutura',
              slug: 'industria-inovacao-e-infraestrutura',
              url: 'https://nacoesunidas.org/pos2015/ods9/'
          },
          {
              number: 10,
              title: 'Redução das Desigualdades',
              slug: 'reducao-das-desigualdades',
              url: 'https://nacoesunidas.org/pos2015/ods10/'
          },
          {
              number: 11,
              title: 'Cidades e Comunidades Sustentáveis',
              slug: 'cidades-e-comunidades-sustentaveis',
              url: 'https://nacoesunidas.org/pos2015/ods11/'
          },
          {
              number: 12,
              title: 'Consumo e Produção Responsáveis',
              slug: 'consumo-e-producao-responsaveis',
              url: 'https://nacoesunidas.org/pos2015/ods12/'
          },
          {
              number: 13,
              title: 'Ação Contra a Mudança Global do Clima',
              slug: 'acao-contra-a-mudanca-global-do-clima',
              url: 'https://nacoesunidas.org/pos2015/ods13/'
          },
          {
              number: 14,
              title: 'Vida na Água',
              slug: 'vida-na-agua',
              url: 'https://nacoesunidas.org/pos2015/ods14/'
          },
          {
              number: 15,
              title: 'Vida Terrestre',
              slug: 'vida-terrestre',
              url: 'https://nacoesunidas.org/pos2015/ods15/'
          },
          {
              number: 16,
              title: 'Paz, Justiça e Instituições Eficazes',
              slug: 'paz-justica-e-instituicoes-eficazes',
              url: 'https://nacoesunidas.org/pos2015/ods16/'
          },
          {
              number: 17,
              title: 'Parcerias e Meios de Implementação',
              slug: 'parcerias-e-meios-de-implementacao',
              url: 'https://nacoesunidas.org/pos2015/ods17/'
          }
      ]
  }
  
  function stringToSlug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
  
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes
  
    return str;
  }
  
  function urlSerialize(data){
      var str = "";
      for (var key in data) {
          if (str != "") {
              str += "&";
          }
          if(typeof data[key] !== "string"){
            for (var k in data[key]){
              if(data[key][k]){
                str += key + "[" + k + "]=" + encodeURIComponent(data[key][k])
                if (str != "") {
                  str += "&";
                }
              }
            }
          }else{
            str += key + "=" + encodeURIComponent(data[key]);
          }
      }
      return str;
    }
  
  
  module.exports = {
    validateEmail,
      xmlToJson,
      timeDifference,
      arrayShuffle,
      numberToReal,
      getOdsList,
      stringToSlug,
      urlSerialize
  }