const TYPES = {
    "individual": 'Pessoa Física',
    "startup": 'Startup',
    "institucional": 'Institucional'
}

function getType(type)
{
    return TYPES[type];
}

module.exports = {
    getType
}