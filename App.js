import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppNavigator from './src/routes/AppNavigator';
import {SafeAreaView} from 'react-navigation'
import * as Font from 'expo-font'

export default class App extends React.Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() 
  {
    await Font.loadAsync({
      'ionicons': require('./src/assets/fonts/Ionicons.ttf'),
      'font-bold': require('./src/assets/fonts/Acumin-BdPro.otf'),
      'lora-bold': require('./src/assets/fonts/Lora-Bold.ttf'),
      'font-italic': require('./src/assets/fonts/Acumin-ItPro.otf'),
      'font-regular': require('./src/assets/fonts/Acumin-RPro.otf'),
      'lora-regular': require('./src/assets/fonts/Lora-Regular.ttf'),
      'font-light': require('./src/assets/fonts/Tahoma-Regular.ttf')
    });

    this.setState({fontLoaded: true})
  }

  render(){
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#440276'}} forceInset={{ bottom: 'never'}}>
        {(this.state.fontLoaded) ? <AppNavigator></AppNavigator> : null}
      </SafeAreaView>
    );
  }
}